from rest_framework import serializers
from rest_api.models import Trade


class TradeSerializer(serializers.ModelSerializer):
    shares = serializers.IntegerField(max_value=100, min_value=1)

    class Meta:
        model = Trade
        fields = ("id", "type", "user_id", "symbol", "shares", "price", "timestamp")
        read_only_fields = ("id",)
