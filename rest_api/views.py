from rest_api.models import Trade
from rest_api.serializers import TradeSerializer
from rest_framework import generics


class TradeList(generics.ListCreateAPIView):
    queryset = Trade.objects.all()
    serializer_class = TradeSerializer
    filter_params = ["type", "user_id", "symbol", "shares", "price", "timestamp"]

    def get_queryset(self):
        queryset = Trade.objects.all()
        filter_dict = {}
        for filter_param in self.filter_params:
            filter_value = self.request.query_params.get(filter_param)
            if filter_value is not None:
                filter_dict[filter_param] = filter_value
        if filter_dict:
            queryset = queryset.filter(**filter_dict)
        return queryset


class TradeDetail(generics.RetrieveAPIView):
    queryset = Trade.objects.all()
    serializer_class = TradeSerializer
