# -*- coding: utf-8 -*-
from django.db import models

TYPES = [("buy", "buy"), ("sell", "sell")]


class Trade(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    type = models.CharField(choices=TYPES, max_length=4, blank=False)
    user_id = models.IntegerField()
    symbol = models.CharField(max_length=50)
    shares = models.IntegerField()
    price = models.IntegerField()
    timestamp = models.IntegerField()
