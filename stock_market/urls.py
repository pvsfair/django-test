from django.urls import path
from rest_api import views

urlpatterns = [
    path("trades/", views.TradeList.as_view()),
    path("trades/<int:pk>", views.TradeDetail.as_view()),
]
